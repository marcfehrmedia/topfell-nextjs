import SEO from '@components/seo'

// Storyblok
import Storyblok, { useStoryblok } from '@utils/storyblok'
import Components from '@components/storyblok/components'

import { PhoneIcon } from '@heroicons/react/24/outline'

import BlurImageBasic from '@components/blur-image-basic'
import LinkComponent from '@components/link'
import ButtonComponent from '@components/button'

export default function Home({ page, preview }) {
	const story = useStoryblok(page, preview)

	return (
		<div className={'bg-center bg-full'}>
			<SEO title={page.content.socialTitle} description={page.content.socialDescription} image={page.content.socialImage} />

			<main className={'h-screen max-h-screen h-screen-ios shadow-xl max-w-full overflow-hidden relative'}>
				{/* Fallback full screen image for mobile */}
				<div className={'block absolute -z-1 w-full min-w-full min-h-full max-w-none h-screen-ios min-h-screen-ios object-cover'}>
					<BlurImageBasic className={'hidden sm:block w-full h-full absolute top-0 left-0'} src={'/header-background.jpg'} layout={'fill'} />
					<BlurImageBasic className={'block sm:hidden w-full h-full absolute top-0 left-0'} src={'/header-background-mobile.jpg'} layout={'fill'} />
				</div>

				{/* Full screen background video */}
				<video autoPlay muted loop className={'hidden sm:block absolute -z-1 w-auto min-w-full min-h-full max-w-none h-screen-ios min-h-screen-ios'}>
					<source src={'/header-video.mp4'} />
				</video>

				<div className={'z-1 pt-12 sm:pt-0 absolute flex justify-center items-center w-full h-full shadow-xl'}>
					<div className={'text-center bg-green-700 bg-opacity-80 sm:bg-opacity-90 px-5 py-3 rounded-xs'}>
						<h1 className={'mb-4 text-stone-50'}>Topfell CC</h1>
						<h2 className={'text-stone-100'}>All Your Tree Needs</h2>
						<div className={'flex gap-x-3 items-center justify-center pt-4 pb-1'}>
							<LinkComponent href={'mailto:topfell@mweb.co.za'} target={'_blank'} className={'text-white hover:text-stone-100 hover:underline'}>
								E-Mail
							</LinkComponent>
							<LinkComponent
								href={'https://www.instagram.com/topfellcc/'}
								target={'_blank'}
								className={'text-white hover:text-stone-100 hover:underline'}
							>
								Instagram
							</LinkComponent>
							<LinkComponent href={'tel:0027217128889'} target={'_blank'} className={'text-white hover:text-stone-100 hover:underline'}>
								Phone
							</LinkComponent>
							<LinkComponent href={'https://wa.me/27610429080'} target={'_blank'} className={'text-white hover:text-stone-100 hover:underline'}>
								WhatsApp
							</LinkComponent>
						</div>
					</div>
				</div>

				<div>{story.content.body ? story.content.body.map((blok) => <Components blok={blok} key={blok._uid} />) : null}</div>

				<ButtonComponent
					href={'tel:0027827707771'}
					className={
						'absolute top-16 text-center left-0 w-full block sm:hidden bg-red-500 hover:bg-red-600 text-white gap-x-2 px-3 py-2 rounded-xs text-base font-bold'
					}
				>
					<div className={'text-center flex items-center justify-center w-full'}>
						<span>Emergency Call</span>
						<PhoneIcon className='w-6 h-6 pl-2' aria-hidden='true' />
					</div>
				</ButtonComponent>
			</main>
		</div>
	)
}

export async function getStaticProps(context) {
	let slug = 'home'

	let params = {
		version: process.env.NODE_ENV === 'development' ? 'draft' : 'published',
	}

	if (context.preview) {
		params.version = 'draft'
		params.cv = Date.now()
	}

	let { data } = await Storyblok.get(`cdn/stories/pages/${slug}`, params)

	return {
		props: {
			page: data ? data.story : false,
			preview: context.preview || false,
		},
		revalidate: 60 * 60,
	}
}
