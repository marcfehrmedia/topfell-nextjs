import Script from 'next/script'
import { useEffect } from 'react'
import * as gtag from '@utils/gtag'
import { useRouter } from 'next/router'
import { Roboto } from '@next/font/google'
import { AppContextProvider } from '@store/app-context'
import { Toaster } from 'react-hot-toast'

import FooterComponent from '@components/footer'
import NavBar from '@components/nav-bar'

import '@styles/globals.css'

const roboto = Roboto({
	weight: ['400', '500', '700', '900'],
	subsets: ['latin'],
})

function MyApp({ Component, pageProps }) {
	const router = useRouter()
	useEffect(() => {
		const handleRouteChange = (url) => {
			gtag.pageview(url)
		}
		router.events.on('routeChangeComplete', handleRouteChange)
		return () => {
			router.events.off('routeChangeComplete', handleRouteChange)
		}
	}, [router.events])

	return (
		<AppContextProvider>
			{/* Global Site Tag (gtag.js) - Google Analytics */}
			<Script strategy='afterInteractive' src={`https://www.googletagmanager.com/gtag/js?id=${gtag.GA_TRACKING_ID}`} />
			<Script
				strategy='afterInteractive'
				dangerouslySetInnerHTML={{
					__html: `
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());
            gtag('config', '${gtag.GA_TRACKING_ID}', {
              page_path: window.location.pathname,
            });
          `,
				}}
			/>
			{/* Additional Google Tag Manager Script */}
			<Script async src="https://www.googletagmanager.com/gtag/js?id=AW-660021717" strategy="afterInteractive" />
			<Script
				strategy="afterInteractive"
				dangerouslySetInnerHTML={{
					__html: `
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());
            gtag('config', 'AW-660021717');
          `,
				}}
			/>
			<main className={`bg-white ${roboto.className}`} key={router.asPath}>
				<NavBar />
				<Component {...pageProps} />
			</main>
			<FooterComponent />
			<Toaster />
		</AppContextProvider>
	)
}

export default MyApp
