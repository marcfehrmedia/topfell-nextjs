import { useState, useEffect } from 'react'

// Next Head for meta information
import Head from 'next/head'
import Masonry, { ResponsiveMasonry } from 'react-responsive-masonry'
import { ZoomImageWithControls } from '@components/zoom-image'
import BlurImage from '@components/storyblok/blur-image'

// The Storyblok Client
import Storyblok, { useStoryblok } from '@utils/storyblok'

// Storyblok components
import Components from '@components/storyblok/components'
import ContactForm from '@components/forms/contact-form'
import SEO from 'components/seo'

const Contact = ({ page, preview }) => {
	const story = useStoryblok(page, preview)

	const [masonryPhotos, setMasonryPhotos] = useState(story.content.photos)
	useEffect(() => {
		setMasonryPhotos(story.content.photos)
	}, [story, masonryPhotos])

	let columnsBreakPoints = { 350: 1, 750: 2, 900: 2, 1280: 3, 1600: 3 }

	if (page.content.photos.length === 1) {
		columnsBreakPoints = { 350: 1, 750: 1, 900: 1, 1280: 1, 1600: 1 }
	}
	if (page.content.photos.length === 2) {
		columnsBreakPoints = { 350: 1, 750: 2, 900: 2, 1280: 2, 1600: 2 }
	}
	if (page.content.photos.length === 3) {
		columnsBreakPoints = { 350: 1, 750: 2, 900: 2, 1280: 2, 1600: 3 }
	}
	if (page.content.photos.length === 4) {
		columnsBreakPoints = { 350: 1, 750: 1, 900: 2, 1280: 3, 1600: 4 }
	}

	console.log(page.content.socialImage)

	return (
		<div className={'mt-16 sm:mt-20'}>
			<SEO title={page.content.socialTitle} description={page.content.socialDescription} image={page.content.socialImage} />
			{story.content.body ? story.content.body.map((blok) => <Components blok={blok} key={blok._uid} />) : null}
			<div className='px-4 py-2 mx-auto bg-white sm:px-6 lg:px-8'>
				<ContactForm />
			</div>
			{masonryPhotos.length > 0 && (
				<div className={'p-5 border-t-4 text-left bg-gray-50'}>
					<h3 className={'mt-0 text-gray-400'}>{story.content.galleryTitle || 'Photo gallery'}</h3>
					{/* <h2 className={'mb-5'}>Projekt-Galerie</h2> */}
					<ResponsiveMasonry columnsCountBreakPoints={columnsBreakPoints}>
						<Masonry gutter={'10px'}>
							{masonryPhotos.map((el) => (
								<ZoomImageWithControls blok={el} key={`zoom-image-${el.uid}`}>
									<BlurImage blok={el} width={640} />
								</ZoomImageWithControls>
							))}
						</Masonry>
					</ResponsiveMasonry>
				</div>
			)}
		</div>
	)
}

export async function getStaticProps(context) {
	let slug = 'contact'

	let params = {
		version: process.env.NODE_ENV === 'development' ? 'draft' : 'published',
	}

	if (context.preview) {
		params.version = 'draft'
		params.cv = Date.now()
	}

	let { data } = await Storyblok.get(`cdn/stories/pages/${slug}`, params)

	return {
		props: {
			page: data ? data.story : false,
			preview: context.preview || false,
		},
		revalidate: 60 * 60,
	}
}

export default Contact
