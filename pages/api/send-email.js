import MailerSend, { Recipient, EmailParams } from 'mailersend';
import slugify from 'react-slugify'

export default async function handler(req, res) {
	if (req.method === 'POST') {
		try {
			const mailersend = new MailerSend({
				api_key: process.env.NEXT_MAILERSEND_ACCESS_TOKEN,
			});

			const data = { ...req.body };

			// Define recipients
			const recipients = [
				new Recipient('topfell@mweb.co.za', 'Topfell CC'),
				new Recipient('marc.fehr@gmail.com', 'Marc Fehr')
			];

			// Check for spam using a bot field
			if (data.botField) {
				console.log('Spam detected! Bot field was not empty.');
				return res.status(400).json({ status: 'error', message: 'Spam detected' });
			}

			// Set up email parameters
			const emailParams = new EmailParams()
				.setFrom(data.email ? `${slugify(data.email)}@pixelpoetry.dev` : 'hello@bearnest.ch')
				.setFromName(`Topfell Form (${data.name || 'No name set'})`)
				.setRecipients(recipients)
				.setSubject('New contact form message')
				.setTemplateId('vywj2lpoo7j47oqz')
				.setPersonalization(recipients.map(recipient => ({
					email: recipient.email,
					data: {
						message: data.message || 'No message entered',
						date: data.date || 'No date set',
						name: data.name || 'No name set',
						email: data.email || 'No email set',
						phone: data.phone || 'No phone set',
						city: data.city || 'No place set',
						address: data.address || 'No address set',
					},
				})));

			// Send the email
			const result = await mailersend.send(emailParams);

			if (result.status === 202) {
				return res.status(200).json({ status: 'Email sent successfully' });
			} else {
				return res.status(500).json({ status: 'error', message: 'Failed to send email' });
			}
		} catch (error) {
			console.error('Error sending email:', error);
			return res.status(500).json({ status: 'error', message: error.message });
		}
	} else {
		return res.status(405).json({ status: 'error', message: 'Method Not Allowed' });
	}
}
