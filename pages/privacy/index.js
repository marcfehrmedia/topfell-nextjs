import SEO from '@components/seo'

export default function PrivacyPage({ page, preview }) {
  return (
    <div className={'mt-16 sm:mt-20 max-w-5xl mx-auto p-16 prose-lg'}>
      <SEO title={`Privacy`} description={`Please read through our Privacy Policy.`} image={null} />

      <h1>Privacy Policy</h1>
      <p><strong>Last Updated:</strong> 4 November 2024</p>

      <p>Welcome to Topfell! We respect your privacy and are committed to protecting any personal information you provide to us. This privacy policy explains how we collect, use, and safeguard your information when you visit our website, use our services, or communicate with us through various channels, including email, WhatsApp, and Facebook Messenger.</p>

      <h2>1. Information We Collect</h2>
      <p>We may collect the following types of personal information:</p>
      <ul>
        <li><strong>Contact Information:</strong> This includes your name, phone number, email address, and any other information you provide when requesting a quote or contacting us directly.</li>
        <li><strong>Service Information:</strong> Details about the tree services you request and any relevant property or access information you share with us.</li>
        <li><strong>Communication Data:</strong> Records of our interactions, including emails, phone calls, WhatsApp messages, and messages on platforms like Facebook Messenger.</li>
        <li><strong>Website Usage Data:</strong> Information collected automatically through cookies and similar technologies, including your IP address, browser type, and pages visited on our website.</li>
      </ul>

      <h2>2. How We Use Your Information</h2>
      <p>The information we collect is used for the following purposes:</p>
      <ul>
        <li><strong>Service Provision:</strong> To provide you with quotes, tree preservation and felling services, and other services you request from us.</li>
        <li><strong>Communication:</strong> To respond to inquiries, arrange appointments, and send important updates about our services.</li>
        <li><strong>Improvement of Services:</strong> To enhance our website, improve customer service, and understand how visitors use our website.</li>
        <li><strong>Marketing:</strong> With your consent, we may send you promotional materials and updates on our services. You can opt-out at any time by contacting us.</li>
      </ul>

      <h2>3. How We Share Your Information</h2>
      <p>We do not sell your personal information to third parties. We may, however, share your information in the following situations:</p>
      <ul>
        <li><strong>With Service Providers:</strong> Trusted third-party providers who assist us with business operations, such as payment processing, email communication, or marketing. They are bound by confidentiality agreements.</li>
        <li><strong>Legal Obligations:</strong> If required by law or in response to valid legal requests, such as complying with court orders or requests from regulatory authorities.</li>
      </ul>

      <h2>4. Your Privacy Rights</h2>
      <p>You have rights regarding your personal data, including:</p>
      <ul>
        <li><strong>Access:</strong> You can request a copy of the information we hold about you.</li>
        <li><strong>Correction:</strong> You can ask us to correct any inaccurate information.</li>
        <li><strong>Deletion:</strong> In certain circumstances, you can request that we delete your personal information.</li>
        <li><strong>Opt-Out of Marketing:</strong> You can unsubscribe from promotional emails or messages at any time.</li>
      </ul>
      <p>To exercise these rights, please contact us at <a href="mailto:topfell@mweb.co.za">topfell@mweb.co.za</a>.</p>

      <h2>5. Data Security</h2>
      <p>We take data security seriously and implement reasonable measures to protect your personal information. However, please remember that no transmission over the internet is entirely secure. While we strive to use acceptable means to protect your data, we cannot guarantee its absolute security.</p>

      <h2>6. Cookies and Tracking</h2>
      <p>Our website uses cookies and similar technologies to enhance user experience, analyse website performance, and remember your preferences. By continuing to use our site, you consent to our use of cookies. You may adjust your browser settings to block cookies if you prefer.</p>

      <h2>7. Children’s Privacy</h2>
      <p>Our services are not directed toward individuals under the age of 18. We do not knowingly collect personal information from minors. If you believe we have inadvertently collected such data, please contact us so we can promptly remove it.</p>

      <h2>8. Updates to This Policy</h2>
      <p>We may update this privacy policy from time to time. Any changes will be posted on this page with the updated date. We encourage you to review this policy periodically to stay informed of how we are protecting your information.</p>

      <h2>9. Contact Us</h2>
      <p>If you have any questions or concerns about this privacy policy or how we handle your data, please reach out to us at:</p>
      <address>
        Topfell CC<br />
        Buitenverwachting, Klein Constantia Rd<br />
        Constantia, Cape Town, 7806<br />
        South Africa<br />
        📞 <a href="tel:+270217128889">+27 021 712 8889</a><br />
        📧 <a href="mailto:topfell@mweb.co.za">topfell@mweb.co.za</a>
      </address>

      <p>Thank you for trusting Topfell with your tree care needs. We are committed to protecting your privacy and ensuring your experience with us is safe and secure.</p>

    </div>
  )
}
