import { useEffect, useContext } from 'react'
import SEO from '@components/seo'

/* UI  */
import Masonry, { ResponsiveMasonry } from 'react-responsive-masonry'
import { ZoomImageWithControls } from '@components/zoom-image'

/* Storyblok */
import Storyblok, { useStoryblok } from '@utils/storyblok'
import Components from '@components/storyblok/components'
import BlurImage from '@components/storyblok/blur-image'

// Navigation
import ServicesBar from 'components/services-bar'

/* Context */
import AppContext from '@store/app-context'

const ServicePage = ({ page, preview }) => {
	page = useStoryblok(page, preview)

	const appContext = useContext(AppContext)

	let columnsBreakPoints = { 350: 1, 750: 2, 900: 2, 1280: 3, 1600: 3 }

	if (page.content.photos.length === 1) {
		columnsBreakPoints = { 350: 1, 750: 1, 900: 1, 1280: 1, 1600: 1 }
	}
	if (page.content.photos.length === 2) {
		columnsBreakPoints = { 350: 1, 750: 2, 900: 2, 1280: 2, 1600: 2 }
	}
	if (page.content.photos.length === 3) {
		columnsBreakPoints = { 350: 1, 750: 2, 900: 2, 1280: 2, 1600: 3 }
	}
	if (page.content.photos.length === 4) {
		columnsBreakPoints = { 350: 1, 750: 1, 900: 2, 1280: 3, 1600: 4 }
	}

	useEffect(() => {
		const lastPath = location.pathname.split('/')[location.pathname.split.length]
		appContext.setCurrentService(`/${lastPath}`)
	}, [])

	useEffect(() => {
		appContext.resetLightboxImages()
	}, [])

	useEffect(() => {
		appContext.addImagesToLightbox(page.content.photos)
	}, [page.content.images, appContext])

	// console.log(appContext.currentService)

	return (
		<div className={'mt-16 sm:mt-20'} key={`service-page-${page.slug}`}>
			<SEO title={page.content.socialTitle} description={page.content.socialDescription} image={page.content.socialImage} />
			<ServicesBar />
			<section className={'storyblok-bloks'}>
				{page.content.body ? page.content.body.map((blok) => <Components blok={blok} key={blok._uid} />) : null}
			</section>
			{page.content.photos.length > 0 && (
				<div className={'p-5 border-t-4 text-left bg-gray-50'}>
					<h3 className={'mt-0 text-gray-400'}>{page.content.galleryTitle || 'Photo gallery'}</h3>
					{/* <h2 className={'mb-5'}>Projekt-Galerie</h2> */}
					<ResponsiveMasonry columnsCountBreakPoints={columnsBreakPoints}>
						<Masonry gutter={'10px'}>
							{page.content.photos.map((el) => (
								<ZoomImageWithControls blok={el} key={`zoom-image-${el.uid}`}>
									<BlurImage blok={el} width={640} />
								</ZoomImageWithControls>
							))}
						</Masonry>
					</ResponsiveMasonry>
				</div>
			)}
		</div>
	)
}

export default ServicePage

export const getStaticProps = async (context) => {
	const slug = context.params.slug

	let params = {
		version: process.env.NODE_ENV === 'development' ? 'draft' : 'published',
	}

	if (context.preview) {
		params.version = 'draft'
		params.cv = Date.now()
	}

	let { data } = await Storyblok.get(`cdn/stories/services/${slug}`, params)

	return {
		props: {
			page: data ? data.story : false,
			preview: context.preview || false,
		},
		revalidate: 60 * 60, // revalidate every hour
	}
}

export const getStaticPaths = async () => {
	let params = {
		version: process.env.NODE_ENV === 'development' ? 'draft' : 'published', // or 'draft' or 'published',
		starts_with: 'services/',
	}

	let { data } = await Storyblok.get(`cdn/stories/`, params)

	const paths = data.stories.map((story) => {
		return {
			params: {
				slug: story.slug,
			},
		}
	})

	return {
		paths: paths,
		fallback: 'blocking', // Creates pages if they don't exist and then stores them...
	}
}
