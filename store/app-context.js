import { createContext, useState, useEffect } from 'react'

const AppContext = createContext({
	lightboxImages: [],
	currentPath: '/',
})

export function AppContextProvider(props) {
	const [currentPath, setCurrentPath] = useState('/')
	const [currentService, setCurrentService] = useState('')
	const [lightboxImages, setLightboxImages] = useState([])
	const [lightboxCurrentImage, setLightboxCurrentImage] = useState(0)

	// useEffect(() => {
	// 	const pathArray = location.pathname.split('/')
	// 	setCurrentService(`/services/${pathArray[pathArray.length - 1]}`)
	// }, [])

	function resetLightboxImages() {
		setLightboxImages([])
	}

	function addImagesToLightbox(bloks) {
		let currentLightboxImages = lightboxImages
		for (let i = 0; i < bloks.length; i++) {
			const found = lightboxImages.some((item) => item.id === bloks[i].id)
			if (!found) {
				currentLightboxImages.push(bloks[i])
			}
		}
		if (lightboxImages !== currentLightboxImages || currentLightboxImages.length === 0) {
			setLightboxImages(currentLightboxImages)
		}
	}

	function setLightboxIndex(currentImage) {
		setLightboxCurrentImage(currentImage)
	}

	const context = {
		currentPath: currentPath,
		setCurrentPath: setCurrentPath,
		currentService: currentService,
		setCurrentService: setCurrentService,
		addImagesToLightbox: addImagesToLightbox,
		lightboxImages: lightboxImages,
		resetLightboxImages: resetLightboxImages,
		setLightboxIndex: setLightboxIndex,
		lightboxCurrentImage: lightboxCurrentImage,
	}

	return <AppContext.Provider value={context}>{props.children}</AppContext.Provider>
}

export default AppContext
