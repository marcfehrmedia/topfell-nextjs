/** @type {import('next').NextConfig} */
const nextConfig = {
	reactStrictMode: true,
	images: {
		domains: ['a.storyblok.com', 'b.storyblok.com', 'img2.storyblok.com'],
	},
}

module.exports = nextConfig
