import Image from 'next/image'

const BlurImage = ({ blok, objectFit, layout, className, width, sizes, priority, style }) => {
	let dimensions = {}

	// Get image dimensions from Storyblok src url
	dimensions = {
		width: parseInt(blok?.filename?.split('/')[5].split('x')[0]),
		height: parseInt(blok?.filename?.split('/')[5].split('x')[1]),
	}

	// Generate placeholder blur image from Storyblok src url
	// function resize(image, option) {
	// 	var imageService = 'https://img2.storyblok.com/'
	// 	var path = image.replace('https://a.storyblok.com', '')
	// 	// console.log(imageService + option + path)
	// 	return imageService + option + path
	// }

	// Calculate size of placeholder (blur) image
	// const resizedImageSize = `${parseInt((50 / dimensions.height) * dimensions.width)}x${parseInt((50 / dimensions.width) * dimensions.height)}`

	// console.log(resizedImageSize)
	// console.log(resize(blok.filename, resizedImageSize))

	return (
		<div className={`${className}`}>
			{blok && layout === 'fill' ? (
				<Image
					src={width ? `${blok.filename}/m/${width}x0` : `${blok.filename}/m/1280x0`}
					sizes={sizes || null}
					alt={blok.alt || 'No alt text set.'}
					fill
					style={...style}
					placeholder={'blur'}
					blurDataURL={'https://topfell-preview.vercel.app/blur-pixel.png'}
					className={`${className} object-cover`}
					priority={`${priority || 'lazy'}`}
					quality={100}
					// blurDataURL={resize(blok.filename, resizedImageSize)}
					// layout={'fill'}
					// objectFit={objectFit || 'contain'}
					// blurDataURL={'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAADUlEQVR42mMUbbCtBwAC1wFTeu0FRgAAAABJRU5ErkJggg=='}
				/>
			) : (
				<Image
					src={width ? `${blok.filename}/m/${width}x0` : `${blok.filename}/m/1280x0`}
					sizes={sizes || null}
					alt={blok.alt || 'No alt text set.'}
					width={dimensions.width}
					style={...style}
					height={dimensions.height}
					placeholder={'blur'}
					blurDataURL={'https://topfell-preview.vercel.app/blur-pixel.png'}
					className={`${className} object-cover`}
					priority={`${priority || 'lazy'}`}
					quality={100}
					// blurDataURL={resize(blok.filename, resizedImageSize)}
					// objectFit={objectFit || 'contain'}
					// layout={layout || 'responsive'}
					// blurDataURL={'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAADUlEQVR42mMUbbCtBwAC1wFTeu0FRgAAAABJRU5ErkJggg=='}
				/>
			)}
		</div>
	)
}

export default BlurImage
