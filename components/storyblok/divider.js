import { sbEditable } from '@storyblok/storyblok-editable'

const Divider = (props) => {
	const { blok } = props

	const text = props.text || blok.text || ''
	const textColor = blok.invisible ? 'text-transparent' : blok.color ? `text-${blok.color}` : `text-gray-400`
	const borderColor = blok.invisible ? 'border-transparent' : blok.color ? `border-${blok.color}` : `border-gray-400`

	return (
		<div
			className={`horizontal-divider relative flex justify-center ${blok.marginTop || 'mt-16'} ${blok.marginBottom} ${blok.className} ${blok.color ? 'h-3' : ''}`}
			{...sbEditable(blok)}
		>
			<div className='absolute inset-x-0 inset-y-0 flex items-center sm:inset-x-16' aria-hidden='true'>
				<div className={`w-full border-none ${borderColor}`} />
			</div>

			<div className='relative flex items-center justify-center'>
				<span className={`text-xs smtext-sm px-1 ${textColor} bg-white`}>{text && !blok.invisible ? text : ''}</span>
			</div>
		</div>
	)
}

export default Divider
