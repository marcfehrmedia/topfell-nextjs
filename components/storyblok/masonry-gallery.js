const MasonryGallery = ({ blok, images }) => {
	return (
		<ResponsiveMasonry columnsCountBreakPoints={columnsBreakPoints}>
			<Masonry {...sbEditable(blok)} gutter={'10px'}>
				{images.map((el) => (
					<ZoomImageWithControls blok={el} key={`zoom-image-${el.uid}`}>
						<BlurImage blok={el} width={640} />
					</ZoomImageWithControls>
				))}
			</Masonry>
		</ResponsiveMasonry>
	)
}

export default MasonryGallery
