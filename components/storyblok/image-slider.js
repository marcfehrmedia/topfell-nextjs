import { useState, useEffect } from 'react'
import { sbEditable } from '@storyblok/storyblok-editable'
import { render } from 'storyblok-rich-text-react-renderer'
import { motion, AnimatePresence } from 'framer-motion'

import BlurImage from '@components/storyblok/blur-image'
import Icon from '@components/icons'

const ImageSlider = ({ blok }) => {
	const swipeConfidenceThreshold = 0

	const swipePower = (offset, velocity) => {
		return Math.abs(offset) * velocity
	}

	const [prevIndex, setPrevIndex] = useState(0)
	const [currentIndex, setCurrentIndex] = useState(0)
	const [isPlaying, setIsPlaying] = useState(false)

	useEffect(() => {
		const interval = setInterval(() => {
			if (currentIndex + 1 >= images?.length) {
				setPrevIndex(-1)
				setCurrentIndex(0)
			} else {
				setPrevIndex(currentIndex)
				setCurrentIndex(currentIndex + 1)
			}
		}, 5000)
		if (!isPlaying) {
			clearInterval(interval)
		}
		return () => clearInterval(interval)
	}, [currentIndex, isPlaying])

	let images = blok.images

	if (blok.imageBlocks?.length > 0) {
		images = blok.imageBlocks
	}
	const fullImages = blok.imageBlocks || null

	const changeImage = (direction) => {
		if (direction === 'next') {
			if (currentIndex + 1 >= images?.length) {
				// return
				setPrevIndex(-1)
				setCurrentIndex(0)
			} else {
				setPrevIndex(currentIndex)
				setCurrentIndex(currentIndex + 1)
			}
		} else {
			if (currentIndex - 1 < 0) {
				setPrevIndex(images.length)
				setCurrentIndex(images?.length - 1)
			} else {
				setPrevIndex(currentIndex)
				setCurrentIndex(currentIndex - 1)
			}
		}
	}

	return (
		<div
			className={`overflow-hidden my-12 max-w-xl px-4 mx-auto sm:px-6 ${fullImages?.length > 1 ? 'md:max-w-4xl' : 'md:max-w-3xl'} lg:px-8 ${blok.backgroundColor}`}
			{...sbEditable(blok)}
		>
			{fullImages?.length > 0 ? (
				<>
					<div className={`relative ${blok.orientation}`} key={`image-slider-${fullImages[currentIndex]._uid}`}>
						<AnimatePresence>
							<motion.div
								key={`image-slide-${fullImages[currentIndex]._uid}`}
								transition={{ duration: 1.5 }}
								animate={{
									opacity: 1,
								}}
								initial={{ opacity: 0 }}
								// leave={{ opacity: 0 }}
								enter={{ opacity: 1 }}
								className={`absolute top-0 left-0 w-full h-full grid grid-cols-${images?.length} grid-flow-row gap-2 sm:gap-4`}
								onMouseEnter={() => setIsPlaying(false)}
								onTapStart={() => setIsPlaying(false)}
								onMouseLeave={() => setIsPlaying(true)}
								onTapCancel={() => setIsPlaying(true)}
							>
								<div>
									<motion.div
										dragElastic={0.5}
										onDragEnd={(e, { offset, velocity }) => {
											const swipe = swipePower(offset.x, velocity.x)
											if (swipe < -swipeConfidenceThreshold) {
												changeImage('next')
											} else if (swipe > swipeConfidenceThreshold) {
												changeImage('back')
											}
										}}
										drag={'x'}
										dragConstraints={{ left: 0, right: 0 }}
										transition={{ ease: 'easeOut', duration: 0.3 }}
										className={`absolute top-0 left-0 inset-0 cursor-pointer shadow-xl hover:shadow-2xl`}
										key={`image-slide-img-${fullImages[currentIndex]._uid}}`}
									>
										{fullImages[currentIndex].image.filename && (
											<BlurImage
												blok={fullImages[currentIndex].image}
												objectFit={'cover'}
												layout={'fill'}
												className={`absolute top-0 left-0 inset-0 w-full h-full pointer-events-none`}
											/>
										)}
									</motion.div>
								</div>
							</motion.div>
						</AnimatePresence>
					</div>
					{fullImages?.length > 1 && (
						<div className={'flex flex-row justify-between cursor-pointer bg-transparent h-20'}>
							<p className={'m-0 p-2'} onClick={() => changeImage('back')}>
								<span className={`w-8 h-5 inline-block ${blok.textColor || 'text-brand-highlight'}`}>
									<Icon icon={'arrow-left'} />
								</span>
							</p>
							{!isPlaying && (
								<p>
									<span className={'italic'}>Slideshow paused</span>
								</p>
							)}
							<p className={'m-0 p-2'} onClick={() => changeImage('next')}>
								<span className={`w-8 h-5 inline-block ${blok.textColor || 'text-brand-highlight'}`}>
									<Icon icon={'arrow-right'} />
								</span>
							</p>
						</div>
					)}
					{fullImages[currentIndex].caption && (
						<motion.div
							key={`image-slide-caption-${fullImages[currentIndex]._uid} `}
							className={'text-center sm:-mt-8 sm:px-11'}
							initial={{ opacity: 0 }}
							animate={{ opacity: 1 }}
							enter={{ opacity: 1 }}
							exit={{ opacity: 0 }}
							transition={{ delay: 0.25 }}
						>
							{render(fullImages[currentIndex].caption)}
						</motion.div>
					)}
				</>
			) : (
				<>
					<AnimatePresence>
						<motion.div
							transition={{ type: 'spring', stiffness: 250, damping: 50 }}
							initial={
								{
									// transform: `translateX(-${currentIndex * (100 / images?.length)}%)`,
								}
							}
							animate={
								{
									// transform: `translateX(-${currentIndex * (100 / images?.length)}%)`,
								}
							}
							className={`image-wrapper-landscape grid grid-cols-${images?.length} relative grid-flow-row gap-2 sm:gap-4`}
							style={{
								// width: `${blok.images?.length * 100}%`,
								zIndex: 1,
							}}
						>
							{images?.map((el, i) => {
								return (
									<motion.div
										dragElastic={0.5}
										onDragEnd={(e, { offset, velocity }) => {
											const swipe = swipePower(offset.x, velocity.x)
											if (swipe < -swipeConfidenceThreshold) {
												changeImage('next')
											} else if (swipe > swipeConfidenceThreshold) {
												changeImage('back')
											}
										}}
										// drag={'x'}
										transition={{ ease: 'easeOut', duration: 0.6 }}
										initial={{
											opacity: 0,
										}}
										animate={{
											opacity: 1,
											zIndex: i === currentIndex ? 999 : 0,
											opacity: i === currentIndex ? 1 : 0,
										}}
										className={`absolute inset-0 cursor-pointer shadow-lg ${i === currentIndex ? 'shadow-xl' : ''} hover:shadow-2xl`}
										key={`image-slide-${el._uid}-${Math.random()}`}
									>
										{el.filename && <BlurImage blok={el} className={`absolute inset-0 w-full h-full pointer-events-none`} />}
									</motion.div>
								)
							})}
						</motion.div>
					</AnimatePresence>
					{images?.length > 1 && (
						<div className={'flex flex-row justify-between cursor-pointer'}>
							<p className={'m-0 p-2'} onClick={() => changeImage('back')}>
								<span className={`w-8 h-5 inline-block ${blok.textColor || 'text-brand-highlight'}`}>
									<Icon icon={'arrow-left'} />
								</span>
							</p>
							<p className={'m-0 p-2'} onClick={() => changeImage('next')}>
								<span className={`w-8 h-5 inline-block ${blok.textColor || 'text-brand-highlight'}`}>
									<Icon icon={'arrow-right'} />
								</span>
							</p>
						</div>
					)}
				</>
			)}
		</div>
	)
}

export default ImageSlider
