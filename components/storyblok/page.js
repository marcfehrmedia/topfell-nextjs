import { sbEditable } from '@storyblok/storyblok-editable'

import Components from '@components/storyblok/components'

const Page = ({ blok }) => (
	<main {...sbEditable(blok)} key={blok._uid}>
		{blok.body ? blok.body.map((blok) => <Components blok={blok} key={blok._uid} />) : null}
	</main>
)

export default Page
