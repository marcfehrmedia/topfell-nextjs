import { sbEditable } from '@storyblok/storyblok-editable'

import BlurImage from '@components/storyblok/blur-image'

const SingleImage = ({ blok }) => {
	return (
		<div {...sbEditable(blok)}>
			<BlurImage blok={blok.image} objectFit={'cover'} layout={'fill'} className={`absolute top-0 left-0 inset-0 w-full h-full pointer-events-none`} />
		</div>
	)
}

export default SingleImage
