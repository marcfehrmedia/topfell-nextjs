import { render } from 'storyblok-rich-text-react-renderer'
import { sbEditable } from '@storyblok/storyblok-editable'

const RichText = ({ blok }) => {
	const { text } = blok

	return (
		<div className={`flex my-6 sm:my-auto ${blok.textColor} ${blok.backgroundColor || 'bg-white'}`} {...sbEditable(blok)}>
			<div className={`max-w-xl w-full px-4 mx-auto sm:px-6 md:max-w-4xl ${blok.paddingY || 'sm:py-0'}`}>
				<div
					className={`mx-auto text-inherit ${blok.proseSizeMobile || 'prose'} ${
						blok.proseSizeDesktop ? `md:${blok.proseSizeDesktop}` : 'md:prose'
					} max-w-none ${blok.textAlign}`}
				>
					{render(text)}
				</div>
			</div>
		</div>
	)
}

export default RichText
