import { sbEditable } from '@storyblok/storyblok-editable'

import Components from '@components/storyblok/components'
import BlurImage from '@components/storyblok/blur-image'

const Hero = ({ blok }) => {
	// console.log(blok.image)

	return (
		<div
			className={`relative px-4 mb-4 sm:mb-10 flex items-center justify-center overflow-hidden ${
				blok.backgroundColor && !blok.image?.filename ? blok.backgroundColor : 'bg-transparent'
			} ${blok.image?.filename ? 'aspect-video' : 'h-auto'} sm:px-0 ${blok.paddingY}`}
			{...sbEditable(blok)}
		>
			{blok.image?.filename && (
				<BlurImage
					blok={blok.image}
					className={'absolute inset-0 -z-1 object-cover w-full bg-center'}
					objectFit={'cover'}
					width={1920}
					style={{ translateY: '-25%' }}
					placeholder={'blur'}
					blurDataURL={'/blur-pixel.png'}
				/>
			)}
			<div className={`py-10 flex flex-col items-center justify-center w-full h-full z-10`}>
				<div
					className={`flex flex-col ${blok.textPosition || 'justify-center'} h-full text-center`}
					style={{ transform: `translateY(${blok.textOffsetY}%)` }}
				>
					<h1
						className={`m-0 leading-loose sm:leading-normal tracking-tighter text-xl sm:text-3xl md:text-4xl lg:text-5xl font-extrabold sm:tracking-tight `}
					>
						<span
							className={`${!blok.title ? 'hidden' : ''} inline box-decoration-clone ${blok.textColor || 'text-br-black-900'} ${
								blok.backgroundColor || 'bg-transparent'
							} ${blok.image?.filename && !blok.backgroundColor ? 'text-shadow-lg' : 'px-3 py-2'}`}
						>
							{blok.title}
						</span>
					</h1>
					{blok.subtitle && (
						<h2 className={`max-w-md mx-auto mt-3 text-base ${blok.textColor || 'text-br-black-500'} sm:text-lg md:mt-5 md:text-xl md:max-w-3xl`}>
							<span
								className={`${blok.image?.filename && !blok.backgroundColor ? 'text-shadow-lg' : 'px-2 py-1'} ${
									blok.backgroundColor || 'bg-transparent'
								} inline box-decoration-clone leading-loose`}
							>
								{blok.subtitle}
							</span>
						</h2>
					)}
				</div>
				{blok.body && blok.body.map((blok) => <Components blok={blok} key={blok._uid} />)}
			</div>
		</div>
	)
}

export default Hero
