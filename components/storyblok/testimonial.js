import { sbEditable } from '@storyblok/storyblok-editable'
import { useState } from 'react'

import { motion } from 'framer-motion'

import BlurImage from '@components/storyblok/blur-image'
import LinkComponent from '@components/link'

const Testimonial = ({ blok }) => {
	const [isHover, setIsHover] = useState(false)

	// console.log(blok.link)

	const spring = {
		type: 'spring',
		damping: 10,
		stiffness: 100,
	}

	return (
		<div
			className={`overflow-hidden bg-transparent ${blok.image?.filename ? 'sm:my-20' : 'sm:my-10'} sm:overflow-visible`}
			{...sbEditable(blok)}
			onMouseEnter={() => setIsHover(true)}
			onMouseLeave={() => setIsHover(false)}
		>
			<div className={`${blok.backgroundColor || 'bg-green-700'} py-10 lg:z-10 lg:relative`}>
				<div className='lg:mx-auto lg:max-w-7xl lg:px-8 lg:grid lg:grid-cols-3 lg:gap-8'>
					{blok.image?.filename && (
						<div className='lg:relative lg:-my-16'>
							<div aria-hidden='true' className='absolute inset-x-0 top-0 hidden bg-white h-1/2 lg:hidden' />
							<motion.div
								className='max-w-md px-4 mx-auto sm:max-w-3xl sm:px-6 lg:p-0 lg:h-full'
								animate={{
									scale: isHover ? 1.025 : 1,
									rotate: isHover ? Math.floor(Math.random() * (2 - -2 + 1) + -2) : 0,
									transition: spring,
								}}
							>
								<motion.div
									className={`relative w-full overflow-hidden ${
										isHover ? 'shadow-2xl' : 'shadow-lg'
									} aspect-video lg:h-full aspect-w-16 aspect-h-9 lg:aspect-none rounded-xs`}
									animate={{
										boxShadow: isHover ? '0 15px 25px -10px rgb(0 0 0 / 0.25)' : '0 1px 2px 0 rgb(0 0 0 / 0.05)',
										transition: spring,
									}}
								>
									<BlurImage
										objectFit={'cover'}
										layout={'fill'}
										blok={blok.image}
										className='absolute object-cover w-full h-full'
										imgClassName={'object-cover overflow-hidden rounded-xs'}
										width={1000}
									/>
								</motion.div>
							</motion.div>
						</div>
					)}
					<div className={`${blok.image?.filename ? 'mt-12 lg:col-span-2 lg:pl-8' : 'lg:col-span-3 px-10'} lg:m-0`}>
						<div className='max-w-md px-4 mx-auto sm:max-w-2xl sm:px-6 lg:px-0 lg:py-16 lg:max-w-none'>
							{/* <blockquote className={`${blok.backgroundColor ? 'border-white' : 'border-l-4'}`}> */}
							<blockquote className={`${blok.borderColor || 'border-white'}`}>
								<div>
									<svg
										className={`w-12 h-12 ${blok.textColor || 'text-white'} fill-current opacity-25' viewBox='0 0 32 32' aria-hidden='true`}
									>
										<path d='M9.352 4C4.456 7.456 1 13.12 1 19.36c0 5.088 3.072 8.064 6.624 8.064 3.36 0 5.856-2.688 5.856-5.856 0-3.168-2.208-5.472-5.088-5.472-.576 0-1.344.096-1.536.192.48-3.264 3.552-7.104 6.624-9.024L9.352 4zm16.512 0c-4.8 3.456-8.256 9.12-8.256 15.36 0 5.088 3.072 8.064 6.624 8.064 3.264 0 5.856-2.688 5.856-5.856 0-3.168-2.304-5.472-5.184-5.472-.576 0-1.248.096-1.44.192.48-3.264 3.456-7.104 6.528-9.024L25.864 4z' />
									</svg>
									{blok.quote && <h3 className={`${blok.textColor || 'text-white'} ${blok.textSize}`}>{blok.quote}</h3>}
								</div>
								<footer className='mt-6'>
									{blok.author && <p className={`text-lg font-bold ${blok.textColor || 'text-white'}`}>{blok.author}</p>}
									{blok.description && blok.link ? (
										<LinkComponent href={blok.link || '/'} className={'text-white underline'}>
											<p className={`text-base font-normal ${blok.textColor || 'text-white'}`}>{blok.description}</p>
										</LinkComponent>
									) : (
										<p className={`text-base font-normal ${blok.textColor || 'text-white'}`}>{blok.description}</p>
									)}
								</footer>
							</blockquote>
						</div>
					</div>
				</div>
			</div>
		</div>
	)
}

export default Testimonial
