import Placeholder from '@components/storyblok/placeholder'
import Page from '@components/storyblok/page'
import Hero from '@components/storyblok/hero'
import RichText from '@components/storyblok/rich-text'
import Divider from '@components/storyblok/divider'
import ImageSlider from '@components/storyblok/image-slider'
import Testimonial from '@components/storyblok/testimonial'
// import MasonryGallery from '@components/storyblok/masonry-gallery'
// import Grid from '@components/storyblok/grid'
// import Features from '@components/storyblok/features'
// import Feature from '@components/storyblok/feature'
// import Button from '@components/storyblok/buttons'
// import ProductList from '@components/storyblok/product-list'
// import ProductHero from '@components/storyblok/product-hero'
// import MasonryGridComponent from '@components/storyblok/masonry-grid'
// import SingleImage from '@components/storyblok/image'
// import Download from '@components/storyblok/download'

const myComponents = {
	page: Page,
	hero: Hero,
	richtext: RichText,
	divider: Divider,
	imageSlider: ImageSlider,
	testimonial: Testimonial,
	// gallery: MasonryGallery,
	// grid: Grid,
	// features: Features,
	// feature: Feature,
	// button: Button,
	// productList: ProductList,
	// productHero: ProductHero,
	// masonryGrid: MasonryGridComponent,
	// download: Download,
	// image: SingleImage,
}

const Components = ({ blok }) => {
	if (typeof myComponents[blok.component] !== 'undefined') {
		const RenderedComponent = myComponents[blok.component]
		return <RenderedComponent blok={blok} />
	}
	return <Placeholder componentName={blok.component} />
}

export default Components
