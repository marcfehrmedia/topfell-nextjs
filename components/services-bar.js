import { useContext } from 'react'
import AppContext from '@store/app-context'
import Link from 'next/link'
import { useRouter } from 'next/router'

const tabs = [
	{ name: 'Bi-Products', href: '/services/bi-products', current: false },
	{ name: 'Emergency Services', href: '/services/emergency-services', current: false },
	{ name: 'Preservation & Repair / Restoration', href: '/services/preservation-repair-restoration', current: false },
	{ name: 'Pruning, Shaping & Maintenance - Preventative', href: '/services/pruning-shaping-maintenance-preventative', current: false },
	{ name: 'Stump Grinding & Excavation', href: '/services/stump-grinding-and-excavation', current: false },
	{ name: 'Transplanting & Planting', href: '/services/transplanting-planting', current: false },
	{ name: 'Tree Felling & Site Clearing', href: '/services/tree-felling-and-site-clearing', current: true },
]

function classNames(...classes) {
	return classes.filter(Boolean).join(' ')
}

export default function ServicesBar() {
	const router = useRouter()
	const appContext = useContext(AppContext)

	const handleClick = (data) => {
		const chosenService = tabs.find((tab) => tab.href === data)
		appContext.setCurrentService(chosenService.href)
		router.push(chosenService.href)
	}

	console.log(appContext.currentService)

	return (
		<div className={'bg-gray-100'}>
			<div className='flex items-center justify-center px-5 sm:px-0 2xl:hidden gap-x-4'>
				<label htmlFor='tabs' className='sr-only'>
					Select a service
				</label>
				{/* I use an "onChange" listener to redirect the user to the selected tab URL. */}
				<select
					key={`select-${appContext.currentService}`}
					id='tabs'
					name='tabs'
					className='block w-full max-w-xl px-3 py-2 my-2 text-base border border-green-700 focus:border-green-500 focus:outline-none focus:ring-green-500 xl:text-sm'
					value={`/services${appContext.currentService}`}
					onChange={(event) => handleClick(event.target.value)}
				>
					<option value='' disabled className={'text-gray-600'} key={`option-${appContext.currentService}-0`}>
						Select a service to read more...
					</option>
					{tabs.map((tab) => (
						<option key={`option-${appContext.currentService}-${tab.name}`} value={tab.href}>
							{tab.name}
						</option>
					))}
				</select>
			</div>
			<div className='hidden 2xl:block'>
				<div className='border-b border-gray-200'>
					<nav className='flex items-center justify-center -mb-px' aria-label='Tabs'>
						{tabs.map((tab) => (
							<Link
								key={tab.name}
								href={tab.href}
								className={classNames(
									tab.href === `/services${appContext.currentService}`
										? 'border-green-500 bg-white text-green-600'
										: 'border-transparent text-gray-500 hover:text-gray-700 hover:bg-white hover:border-green-500',
									'whitespace-nowrap py-4 px-3 border-b-2 font-medium text-sm'
								)}
								aria-current={tab.current ? 'page' : undefined}
								onClick={() => appContext.setCurrentService(tab.href)}
							>
								{tab.name}
							</Link>
						))}
					</nav>
				</div>
			</div>
		</div>
	)
}
