import Link from 'next/link'

const LinkComponent = ({ children, href, target, className }) => {
	return (
		<Link className={className || 'underline'} href={href || '#'} target={target || '_self'} rel={target === '_blank' ? 'noopener noreferrer' : null}>
			{children}
		</Link>
	)
}

export default LinkComponent
