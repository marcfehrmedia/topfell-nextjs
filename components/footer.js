import { EnvelopeIcon, MapIcon, ChatBubbleLeftRightIcon } from '@heroicons/react/24/outline'

import ButtonComponent from '@components/button'
import LinkComponent from '@components/link'

const FooterComponent = () => {
	return (
		<footer className={'bg-green-700 text-white'}>
			<div className={'flex py-8 flex-col-reverse sm:flex-row justify-between mx-auto max-w-7xl px-5 sm:px-6 lg:px-8 sm:items-center gap-y-8'}>
				<div className={'flex flex-col gap-y-4'}>
					<p>
						<strong>Topfell CC</strong>
						<br />
						Buitenverwachting
						<br />
						Klein Constantia Rd
						<br />
						Constantia, Cape Town, 7806
						<br />
						South Africa
					</p>
					<ButtonComponent
						href={'https://goo.gl/maps/RZvKWP1dPBgCYmTq5?hl=en'}
						className={'bg-green-600 hover:bg-green-500 text-white'}
						target={'_blank'}
					>
						<span>Google Maps</span> <MapIcon className='w-6 h-6' aria-hidden='true' />
					</ButtonComponent>
				</div>
				<div className={'sm:text-center'}>
					<p class="mb-3">📞 Call our office: <a href="tel:0027217128889" class="font-bold hover:underline">+27 021 712 8889</a></p>
					<p>
						For any requests please contact{' '}
						<LinkComponent href='mailto:topfell@mweb.co.za' target='_blank' rel='noopener noreferrer'>
							topfell@mweb.co.za
						</LinkComponent>
						.
					</p>
					<p>© {new Date().getFullYear()} All rights reserved to Topfell CC</p>
				</div>
				<div className={'flex flex-col gap-4 items-start sm:items-end'}>
					<ButtonComponent target={'_blank'} href={'mailto:topfell@mweb.co.za'} className={'bg-green-600 hover:bg-green-500 text-white'}>
						<span>Get a quote by email</span> <EnvelopeIcon className='w-6 h-6' aria-hidden='true' />
					</ButtonComponent>
					<ButtonComponent target={'_blank'} href={'https://wa.me/27610429080'} className={'bg-green-600 hover:bg-green-500 text-white'}>
						<span>Chat to us on WhatsApp</span> <ChatBubbleLeftRightIcon className='w-6 h-6' aria-hidden='true' />
					</ButtonComponent>
				</div>
			</div>
		</footer>
	)
}

export default FooterComponent
