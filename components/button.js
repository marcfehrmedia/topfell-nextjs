import Link from 'next/link'

const ButtonComponent = ({ className, children, href, target }) => {
	return (
		<Link href={href || '#'} target={target || '_self'} rel={target === '_blank' ? 'noopener noreferrer' : null}>
			<button
				className={
					className
						? `flex gap-x-2 px-3 py-2 rounded-xs text-base font-bold ${className}`
						: 'flex bg-red-500 group-hover:bg-red-600 text-white gap-x-2 px-3 py-2 rounded-xs text-base font-bold'
				}
			>
				{children}
			</button>
		</Link>
	)
}

export default ButtonComponent
