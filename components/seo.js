import Head from 'next/head'

const SEO = ({ title, description, image, url }) => {
	function resize(image, option) {
		var imageService = 'https://img2.storyblok.com/'
		var path = image?.replace('https://a.storyblok.com', '')
		return imageService + option + path
	}

	return (
		<Head>
			<title>{title || 'Topfell CC | All Your Tree Needs'}</title>
			<link rel='icon' href='/favicon.ico' />
			<meta property='og:url' content={url || 'https://www.topfell.co.za'} />
			<meta property='og:type' content='website' />
			<meta
				property='og:image'
				content={`${image?.filename ? resize(image?.filename, '640x0') : 'https://img2.storyblok.com/640x0/f/183263/1920x1440/f27a066400/header-background.jpg'}`}
			/>
			{description && (
				<>
					<meta name='description' content={description} />
					<meta property='og:description' content={description} />
					<meta name='twitter:description' content={description} />
				</>
			)}
		</Head>
	)
}

export default SEO
