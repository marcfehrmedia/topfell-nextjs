import toast from 'react-hot-toast'

import { useState, useRef, useContext } from 'react'

import AppContext from '@store/app-context'

function ContactForm() {
	const [error, setError] = useState({})
	const [isSent, setIsSent] = useState(false)

	const firstnameInputRef = useRef()
	const lastnameInputRef = useRef()
	const phoneInputRef = useRef()
	const placeInputRef = useRef()
	const emailInputRef = useRef()
	const messageInputRef = useRef()
	const botInputRef = useRef()
	const addressInputRef = useRef()

	const appContext = useContext(AppContext)

	async function postData(url = '', data = {}) {
		const response = await fetch(url, {
			method: 'POST',
			cache: 'no-cache',
			headers: {
				'Content-Type': 'application/json',
			},
			referrerPolicy: 'no-referrer',
			body: JSON.stringify(data),
		})
		return response
	}

	const processMessage = (event) => {
		event.preventDefault()

		// Validation to be done here TODO
		// If data valid, send POST request

		if (
			messageInputRef.current.value !== '' &&
			emailInputRef.current.value !== '' &&
			phoneInputRef.current.value !== '' &&
			placeInputRef.current.value !== '' &&
			firstnameInputRef.current.value !== '' &&
			lastnameInputRef.current.value !== '' &&
			addressInputRef.current.value !== '' &&
			botInputRef.current.value === ''
		) {
			postData('/api/send-email', {
				message: messageInputRef.current.value,
				email: emailInputRef.current.value,
				phone: phoneInputRef.current.value,
				city: placeInputRef.current.value,
				name: `${firstnameInputRef.current.value} ${lastnameInputRef.current.value}`,
				botField: botInputRef.current.value,
				address: addressInputRef.current.value,
			})
				.then(async (res) => {
					if (res.ok) {
						return res.json()
					}
					const data = await res.json()
					throw new Error(data.message || 'Something went wrong...')
				})
				.then((data) => {
					toast.success('Message sent!')
					setIsSent(true)
				})
				.catch((error) => {
					toast.error('Something went wrong.')
				})
		}
	}

	return (
		<div className={'max-w-xl md:max-w-4xl mx-auto'}>
			{isSent && (
				<div
					className={
						'tet-center w-full px-5 py-3 mt-36 mb-40 transition-opacity border-green-700 border-4 text-green-700 prose-xl text-center font-bold'
					}
				>
					<h4>👏 Thanks, we received your message. We'll get back to you as soon as possible.</h4>
				</div>
			)}
			{!isSent && (
				<form className='w-full pb-8' onSubmit={processMessage}>
					<input disabled={isSent} type='hidden' name='form-name' value='contact' />
					<p className='hidden'>
						<label>
							Don’t fill this out if you’re human: <input name='bot-field' ref={botInputRef} />
						</label>
					</p>
					<div className='flex flex-wrap mt-8 -mx-3'>
						<div className='w-full px-3 sm:mb-6 md:w-1/2 md:mb-0'>
							<label className='block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase' htmlFor='grid-first-name'>
								First name *
							</label>
							<input
								disabled={isSent}
								ref={firstnameInputRef}
								className='block w-full px-4 py-3 mb-3 leading-tight text-gray-700 bg-white border border-gray-400 appearance-none rounded-xs focus:outline-none focus:bg-white'
								id='grid-first-name'
								type='text'
								name='vorname'
								placeholder='James'
								autoComplete='firstname'
							/>
							{error.firstname && <p className='text-xs italic text-brand'>Please fill out this field.</p>}
						</div>

						<div className='w-full px-3 md:w-1/2'>
							<label className='block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase' htmlFor='grid-last-name'>
								Name *
							</label>
							<input
								disabled={isSent}
								ref={lastnameInputRef}
								className='block w-full px-4 py-3 leading-tight text-gray-700 bg-white border border-gray-400 appearance-none rounded-xs focus:outline-none focus:bg-white focus:border-gray-500'
								id='grid-last-name'
								type='text'
								name='nachname'
								placeholder='Smith'
								autoComplete='lastname'
							/>
							{error.lastname && <p className='text-xs italic text-brand'>Please fill out this field.</p>}
						</div>
					</div>

					<div className='flex flex-wrap -mx-3'>
						<div className='w-full px-3 mt-1 sm:mb-6 md:w-1/2 md:mb-0'>
							<label className='block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase' htmlFor='grid-phone'>
								Phone / WhatsApp *
							</label>
							<input
								disabled={isSent}
								ref={phoneInputRef}
								autoComplete='phone'
								className='block w-full px-4 py-3 mb-3 leading-tight text-gray-700 bg-white border border-gray-400 appearance-none rounded-xs focus:outline-none focus:bg-white'
								id='grid-phone'
								type='phone'
								name='telefon'
								placeholder='079 123 66 66'
							/>
							{error.firstname && <p className='text-xs italic text-brand'>Please fill out this field.</p>}
						</div>

						<div className='w-full px-3 mt-1 md:w-1/2 sm:mt-1'>
							<label className='block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase' htmlFor='grid-place'>
								City / Town
							</label>
							<input
								disabled={isSent}
								ref={placeInputRef}
								autoComplete='town'
								className='block w-full px-4 py-3 leading-tight text-gray-700 bg-white border border-gray-400 appearance-none rounded-xs focus:outline-none focus:bg-white focus:border-gray-500'
								id='grid-place'
								type='text'
								name='wohnort'
								placeholder='Cape Town'
							/>
							{error.lastname && <p className='text-xs italic text-brand'>Please fill out this field.</p>}
						</div>
					</div>

					<div className='flex flex-wrap mb-1 -mx-3'>
						<div className='w-full px-3'>
							<label className='block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase' htmlFor='grid-email'>
								Address
							</label>
							<input
								disabled={isSent}
								autoComplete='address'
								ref={addressInputRef}
								className='block w-full px-4 py-3 mb-3 leading-tight text-gray-700 bg-white border border-gray-400 appearance-none rounded-xs focus:outline-none focus:bg-white focus:border-gray-500'
								id='grid-address'
								type='text'
								name='address'
								placeholder={'12 Kloof street'}
							/>
							<p className='text-xs italic text-gray-600'>{error.email && <p className='text-xs italic text-brand'>Please enter a valid email address</p>}</p>
						</div>
					</div>

					<hr className={'my-5'} />

					<div className='flex flex-wrap mb-1 -mx-3'>
						<div className='w-full px-3'>
							<label className='block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase' htmlFor='grid-email'>
								E-mail*
							</label>
							<input
								disabled={isSent}
								autoComplete='email'
								ref={emailInputRef}
								className='block w-full px-4 py-3 mb-3 leading-tight text-gray-700 bg-white border border-gray-400 appearance-none rounded-xs focus:outline-none focus:bg-white focus:border-gray-500'
								id='grid-email'
								type='email'
								name='email'
								placeholder={'your.name@gmail.com'}
							/>
							<p className='text-xs italic text-gray-600'>{error.email && <p className='text-xs italic text-brand'>Bitte gültige E-Mail-Adresse angeben</p>}</p>
						</div>
					</div>

					<div className='flex flex-wrap mb-6 -mx-3'>
						<div className='w-full px-3'>
							<label className='block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase' htmlFor='grid-message'>
								Your message *
							</label>
							<textarea
								disabled={isSent}
								ref={messageInputRef}
								placeholder={'Please let us know how we can assist you.'}
								className='block w-full h-48 px-4 py-3 mb-3 leading-tight text-gray-700 bg-white border border-gray-400 appearance-none resize-none rounded-xs no-resize focus:outline-none focus:bg-white focus:border-gray-500'
								id='grid-message'
								name='message'
							/>
							{error.message === '' && <p className='text-xs italic text-brand'>Please enter a message</p>}
						</div>
					</div>

					<p className={'opacity-80 italic -mt-5 text-center sm:text-left sm:-mt-2 sm:pb-4'}>* Mandatory fields</p>

					{!isSent && (
						<div className='text-center md:flex md:items-center'>
							<div className='md:w-full'>
								<button
									className='px-4 py-2 font-bold text-white bg-green-600 border-none shadow hover:shadow-lg rounded-xs hover:bg-green-500 focus:shadow-outline focus:outline-none'
									type='submit'
								>
									Send message
								</button>
							</div>
						</div>
					)}
				</form>
			)}
		</div>
	)
}

export default ContactForm
