import { useContext, useState } from 'react'

import AppContext from '@store/app-context'
import { motion, AnimatePresence } from 'framer-motion'
import useKeypress from 'react-use-keypress'
import BlurImage from '@components/storyblok/blur-image'
import Icon from '@components/icons'

const ImageOverlay = ({ blok, closeHandler }) => {
	const appContext = useContext(AppContext)

	const currentImage = appContext.lightboxImages.filter((el) => el.filename === blok.filename)[0]
	let currentImageIdx = appContext.lightboxImages.indexOf(currentImage)

	const [currentImageIndex, setCurrentImageIndex] = useState(currentImageIdx)

	useKeypress(['ArrowLeft', 'ArrowRight'], (event) => {
		if (event.key === 'ArrowLeft') {
			nextImage()
		}
		if (event.key === 'ArrowRight') {
			prevImage()
		}
	})

	const nextImage = () => {
		if (currentImageIndex + 1 >= appContext.lightboxImages.length) {
			// Start with index -1
			setCurrentImageIndex(0)
		} else {
			setCurrentImageIndex(currentImageIndex + 1)
		}
	}

	const prevImage = () => {
		if (currentImageIndex === 0) {
			// Start with index -1
			setCurrentImageIndex(appContext.lightboxImages.length - 1)
		} else {
			setCurrentImageIndex(currentImageIndex - 1)
		}
	}

	return (
		<AnimatePresence>
			<div className={'fixed top-0 left-0 w-full bg-white bottom-0 right-0 p-8 flex justify-center items-center flex-col z-50 cursor-default'}>
				<motion.div
					key={appContext.lightboxImages[currentImageIndex].id}
					initial={{ opacity: 0 }}
					animate={{ opacity: [0, 1] }}
					enter={{ opacity: [0, 1] }}
					exit={{ opacity: 0 }}
					className={'absolute flex cursor-zoom-out w-full h-full'}
					onClick={() => closeHandler()}
					style={{
						maxWidth: '85vw',
						maxHeight: '85vh',
						// maxWidth: dimensions.width > dimensions.height ? `calc(100 / 9 * 16)%` : `calc(100/16*9)%`,
						// maxHeight: `calc(${dimensions.width} / ${dimensions.height * 100}%`,
					}}
				>
					{appContext.lightboxImages.map((el, i) => (
						<BlurImage // Visible images
							className={`w-full relative mx-auto ${i === currentImageIndex ? 'z-1' : 'hidden -z-1'}`}
							blok={el}
							width={1920}
							layout={'fill'}
							objectFit={'contain'}
							key={`blur-image-${i}`}
							// priority={'eager'}
						/>
					))}
				</motion.div>
				<div onClick={() => closeHandler()} className={'z-20'}>
					<CloseIcon />
				</div>
				<div className={'absolute bottom-0 left-0 w-full flex flex-row justify-between cursor-pointer'}>
					<p onClick={() => nextImage()} className={'m-0 p-5'}>
						<span className={`w-8 h-5 inline-block ${blok.textColor || 'text-black'} hover:text-brand`}>
							<Icon icon={'arrow-left'} />
						</span>
					</p>
					<p className={'m-0 p-5'} onClick={() => prevImage()}>
						<span className={`w-8 h-5 inline-block ${blok.textColor || 'text-black'} hover:text-brand`}>
							<Icon icon={'arrow-right'} />
						</span>
					</p>
				</div>
			</div>
		</AnimatePresence>
	)
}

const CloseIcon = () => {
	return (
		<div className={'absolute top-2 right-2 text-black hover:text-brand cursor-pointer w-12 h-12 z-50'}>
			<AnimatePresence>
				<svg xmlns='http://www.w3.org/2000/svg' width='100%' className={'fill-current w-full inline'} fill='none' viewBox='0 0 385 385'>
					<motion.rect
						width='333'
						height='65'
						x='26'
						y='160'
						className={'fill-current'}
						rx='13'
						animate={{ transform: `rotate(-405deg)` }}
						initial={{ transform: `rotate(0deg)` }}
						transition={{ duration: 0.3 }}
					/>
					<motion.rect
						width='333'
						height='65'
						x='26'
						y='160'
						className={'fill-current'}
						rx='13'
						animate={{ transform: `rotate(-135deg)` }}
						initial={{ transform: `rotate(0deg)` }}
						transition={{ duration: 0.2 }}
					/>
				</svg>
			</AnimatePresence>
		</div>
	)
}

export const ZoomImageWithControls = ({ blok, cursorClassName, children }) => {
	const [isOpen, setIsOpen] = useState(false)

	let isEditor = false

	if (typeof window !== 'undefined') {
		isEditor = window.location.href.indexOf('_storyblok') > -1
	}

	useKeypress('Escape', () => {
		if (!isOpen) {
			return
		}
		toggleImageLightbox()
	})

	const toggleImageLightbox = () => {
		if (isEditor) return
		if (isOpen) {
			// Release body scroll
			setIsOpen(false)
			document.querySelector('html').style.overflowY = 'scroll'
		} else {
			// Fixate body scroll
			setIsOpen(true)
			document.querySelector('html').style.overflowY = 'hidden'
		}
	}

	return (
		<div
			className={`${cursorClassName || 'cursor-pointer'} group`}
			onClick={() => {
				!isOpen ? toggleImageLightbox() : null
			}}
		>
			{children}
			{isOpen && <ImageOverlay blok={blok} closeHandler={toggleImageLightbox} />}
		</div>
	)
}

const ZoomImage = ({ blok, cursorClassName, children }) => {
	const [isOpen, setIsOpen] = useState(false)

	let isEditor = false

	if (typeof window !== 'undefined') {
		isEditor = window.location.href.indexOf('_storyblok') > -1
	}

	const toggleImageLightbox = () => {
		if (isEditor) {
			return
		}
		if (isOpen) {
			// Release body scroll
			setIsOpen(false)
			document.querySelector('html').style.overflowY = 'scroll'
		} else {
			// Fixate body scroll
			setIsOpen(true)
			document.querySelector('html').style.overflowY = 'hidden'
		}
	}

	return (
		<div className={`${cursorClassName || 'cursor-pointer'} group`} key={Math.random()}>
			{children}
			{isOpen && <ImageOverlay blok={blok} closeHandler={() => toggleImageLightbox} />}
		</div>
	)
}

export default ZoomImage
