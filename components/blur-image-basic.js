import Image from 'next/image'

const BlurImageBasic = ({ src, blurImage, layout, className, width, height, alt, objectFit }) => {
	return (
		<div className={`${className} overflow-hidden`}>
			{layout === 'fill' ? (
				<Image
					src={src}
					alt={alt || 'No alt text set.'}
					objectFit={objectFit || 'cover'}
					layout={layout || 'responsive'}
					placeholder={blurImage ? 'blur' : null}
					blurDataURL={blurImage || null}
					className={`${className} overflow-hidden`}
				/>
			) : (
				<Image
					src={src}
					alt={alt || 'No alt text set.'}
					objectFit={objectFit || 'cover'}
					width={layout === 'fill' ? null : parseInt(width)}
					height={layout === 'fill' ? null : parseInt(height)}
					layout={layout || 'responsive'}
					placeholder={blurImage ? 'blur' : null}
					blurDataURL={blurImage || null}
					className={`${className} overflow-hidden`}
				/>
			)}
		</div>
	)
}

export default BlurImageBasic
