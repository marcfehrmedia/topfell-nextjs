import { useContext } from 'react'
import AppContext from '@store/app-context'
import Link from 'next/link'
import { Disclosure } from '@headlessui/react'
import { Bars3Icon, PhoneIcon, XMarkIcon, EnvelopeIcon } from '@heroicons/react/24/outline'
import LinkComponent from './link'
import ButtonComponent from './button'

const navigation = [
	{ name: 'Home', href: '/', desktop: false, mobile: true },
	{ name: 'About us', href: '/about-us', desktop: true, mobile: true },
	{ name: 'Services', href: '/services', desktop: true, mobile: true },
	{ name: 'Bi-Products', href: '/services/bi-products', desktop: true, mobile: true },
	{ name: 'Gallery', href: '/gallery', desktop: true, mobile: true },
	{ name: 'Contact', href: '/contact', desktop: true, mobile: true },
]

function classNames(...classes) {
	return classes.filter(Boolean).join(' ')
}

export default function NavBar() {
	const appContext = useContext(AppContext)

	return (
		<Disclosure as='nav' className='absolute top-0 left-0 z-50 w-full h-16 bg-white shadow-xl sm:h-20' key={appContext.currentPath}>
			{({ open }) => (
				<>
					<div className='px-2 mx-auto max-w-7xl sm:px-6 lg:px-8'>
						<div className='relative flex items-center justify-between h-16 sm:h-20'>
							<div className='absolute inset-y-0 left-0 flex items-center sm:hidden'>
								<Disclosure.Button className='inline-flex items-center justify-center p-2 text-gray-400 rounded-xs hover:bg-gray-700 hover:text-white focus:outline-none focus:ring-2 focus:ring-inset focus:ring-white'>
									<span className='sr-only'>Open main menu</span>
									{open ? (
										<XMarkIcon className='block w-6 h-6' aria-hidden='true' />
									) : (
										<Bars3Icon className='block w-6 h-6' aria-hidden='true' />
									)}
								</Disclosure.Button>
							</div>
							<ButtonComponent
								href={'/contact'}
								// target={'_blank'}
								className={'hidden lg:flex bg-green-700 hover:bg-green-600 text-white gap-x-2 px-3 py-2 rounded-xs text-base font-bold'}
							>
								<span>Get a quote</span>
								<EnvelopeIcon className='w-6 h-6' aria-hidden='true' />
							</ButtonComponent>
							<div
								className='flex flex-col items-center justify-center flex-1 w-full pt-1 sm:items-center sm:justify-center sm:ml-6'
								onClick={() => appContext.setCurrentPath('/')}
							>
								<LinkComponent href={'/'}>
									<div className='flex items-center flex-shrink-0'>
										<img className='block w-auto h-10 lg:hidden' src='/logo-text.svg' alt='Your Company' />
										<img className='hidden w-auto h-10 lg:block' src='/logo-text.svg' alt='Your Company' />
									</div>
								</LinkComponent>
								{navigation.length > 0 && (
									<div className='hidden sm:ml-6 sm:block'>
										<div className='flex space-x-4'>
											{navigation
												.filter((el) => el.desktop)
												.map((item) => (
													<Link
														key={`desktop-link-item-${item.href}`}
														href={item.href}
														className={classNames(
															item.href === appContext.currentPath
																? 'text-green-700'
																: 'text-gray-800 hover:text-green-700 hover:underline',
															'px-3 py-1 rounded-xs text-base font-bold'
														)}
														aria-current={item.href === appContext.currentPath ? 'page' : undefined}
													>
														{item.name}
													</Link>
												))}
										</div>
									</div>
								)}
							</div>
							<div className='absolute inset-y-0 right-0 flex items-center pr-2 sm:static sm:inset-auto sm:ml-6 sm:pr-0'>
								<ButtonComponent
									href={'tel:+27827707771'}
									className={'hidden lg:flex bg-red-500 hover:bg-red-600 text-white gap-x-2 px-3 py-2 rounded-xs text-base font-bold'}
									target={'_blank'}
								>
									<span>Emergency Call</span>
									<PhoneIcon className='w-6 h-6' aria-hidden='true' />
								</ButtonComponent>
							</div>
						</div>
					</div>

					<Disclosure.Panel className='bg-white sm:hidden'>
						<div className='flex flex-col px-2 py-2 space-y-1 divide-y shadow-lg'>
							{navigation
								.filter((el) => el.mobile)
								.map((item) => (
									<Link href={item.href} key={`mobile-link-item-${item.href}`}>
										<Disclosure.Button
											key={item.name}
											as='span'
											href={item.href}
											className={classNames(
												item.href === appContext.currentPath
													? 'text-green-700 underline'
													: 'text-gray-700 hover:bg-green-700 hover:text-white',
												'block px-3 py-3 rounded-xs text-base font-bold  text-center'
											)}
											aria-current={item.href === appContext.currentPath ? 'page' : undefined}
											onClick={() => appContext.setCurrentPath(item.href)}
										>
											{item.name}
										</Disclosure.Button>
									</Link>
								))}
						</div>
					</Disclosure.Panel>
				</>
			)}
		</Disclosure>
	)
}
