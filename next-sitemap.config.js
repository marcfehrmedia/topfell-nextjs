module.exports = {
	siteUrl: process.env.SITE_URL || 'https://topfell.co.za',
	generateRobotsTxt: true, // (optional)
	// ...other options
}
